package com.example.assignment;

import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.assignment.adapter.UserListAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;
    ListView lvusers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewReferances();
        initBindView();
    }

    void initBindView() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserDetails"));
        userListAdapter = new UserListAdapter(this, userList);
        lvusers.setAdapter(userListAdapter);
    }


    void initViewReferances() {
        lvusers = findViewById(R.id.lvActListViewOfUser);
    }
}
