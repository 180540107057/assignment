package com.example.assignment.util;

public class Const {
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String Email_ADDRESS = "EmailAddress";
    public static final String Gender = "Gender";
    public static final String HOBBY = "Hobby";
}
