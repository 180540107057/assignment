package com.example.assignment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.assignment.R;
import com.example.assignment.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.String.valueOf;

public class UserListAdapter extends BaseAdapter {


    Context context;
    ArrayList<HashMap<String, Object>> userList;

    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;

    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_raw_user_list, null);
        TextView tvNameView = view1.findViewById(R.id.tvActNameView);
        TextView tvEmailView = view1.findViewById(R.id.tvActEmailView);
        TextView tvGenderView = view1.findViewById(R.id.tvActGenderView);
        TextView tvHobbyView = view1.findViewById(R.id.tvActHobbyView);

        tvNameView.setText(userList.get(position).get(Const.FIRST_NAME) + " " + userList.get(position).get(Const.LAST_NAME));
        tvEmailView.setText((valueOf(userList.get(position).get(Const.Email_ADDRESS))));
        tvHobbyView.setText((valueOf(userList.get(position).get(Const.HOBBY))));

        if (valueOf(userList.get(position).get(Const.Gender)).contains("Female")) {
            tvGenderView.setBackgroundResource(R.drawable.background_for_female_gender);
            tvGenderView.setText("F");
        }

        return view1;
    }
}


