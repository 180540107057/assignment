package com.example.assignment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.assignment.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    EditText etName, etPhoneNo, etEmail, etLastName;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    Button btnSubmit;
    CheckBox cbCricket, cbFootball, cbHocky;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();


//        choosing checkbox by Gender
        if (rbFemale.isChecked()) {
            cbHocky.setVisibility(View.GONE);
        }

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActFemale) {
                    cbHocky.setVisibility(View.GONE);
                } else {
                    cbHocky.setVisibility(View.VISIBLE);
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, etName.getText().toString());
                    map.put(Const.LAST_NAME, etLastName.getText().toString());
                    map.put(Const.Email_ADDRESS, etEmail.getText().toString());
                    map.put(Const.Gender, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());

//                     checking for hobbies
                    String HobbyTemp = "";
                    if (cbCricket.isChecked() && (cbFootball.isChecked() || cbHocky.isChecked())) {
                        HobbyTemp += cbCricket.getText().toString() + ",";
                    } else if (cbCricket.isChecked()) {
                        HobbyTemp += cbCricket.getText().toString();
                    }

//                      checking for football

                    if (cbFootball.isChecked() && cbHocky.isChecked()) {
                        HobbyTemp += cbFootball.getText().toString() + ",";
                    } else if (cbFootball.isChecked()) {
                        HobbyTemp += cbFootball.getText().toString();
                    }

//                     checking for hocky

                    if (cbHocky.isChecked()) {
                        HobbyTemp += cbHocky.getText().toString();
                    }
                    map.put(Const.HOBBY, HobbyTemp);
                    userList.add(map);
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("UserDetails", userList);
                    startActivity(intent);
                }
            }
        });
    }

    void initViewReference() {
        etName = findViewById(R.id.etActName);
        etPhoneNo = findViewById(R.id.etActPhoneno);
        etEmail = findViewById(R.id.etActEmail);
        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);
        cbCricket = findViewById(R.id.cbActCricket);
        cbFootball = findViewById(R.id.cbActFootball);
        cbHocky = findViewById(R.id.cbActHockey);
        btnSubmit = findViewById(R.id.btnActSubmit);
        etLastName = findViewById(R.id.etActLastName);
    }

    //validation
    Boolean isValid() {
        boolean flag = true;
        String LastName = etLastName.getText().toString().trim();
        String Name = etName.getText().toString().trim();
        if (TextUtils.isEmpty(etName.getText())) {
            etName.setError("plz enter name");
            flag = false;
            etName.requestFocus();
        } else if (!(Name.matches("[a-zA-z/S]+"))) {
            etName.setError("plz enter valid name");
            flag = false;
            etName.requestFocus();
        }
        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError("plz enter name");
            flag = false;
            etLastName.requestFocus();
        } else if (!(LastName.matches("[a-zA-z/S]+"))) {
            etLastName.setError("plz enter valid name");
            flag = false;
            etLastName.requestFocus();
        }

        String PhoneNo = etPhoneNo.getText().toString();
        if (TextUtils.isEmpty(etPhoneNo.getText())) {
            etPhoneNo.setError("plz enter phone no");
            flag = false;
            etPhoneNo.requestFocus();
        } else if (PhoneNo.length() < 10) {
            etPhoneNo.setError("plz enter valid phone no");
            flag = false;
            etPhoneNo.requestFocus();
        }


        String EmailAddress = etEmail.getText().toString().trim();
        String EmailPattern = "[a-zA-Z0-9._-]+@[a-z.]+\\.+[a-z]+";
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError("plz enter Email");
            flag = false;
            etEmail.requestFocus();
        } else {
            if (!(EmailAddress.matches(EmailPattern))) {
                etEmail.setError("plz enter valid  Email");
                flag = false;
                etEmail.requestFocus();
            }
        }
//         hobby validation
        if (!(cbCricket.isChecked() || cbFootball.isChecked() || cbHocky.isChecked())) {
            Toast.makeText(getBaseContext(), "please choose any one", Toast.LENGTH_LONG).show();
            flag = false;
        }
        if (!(rbFemale.isChecked() || rbMale.isChecked())) {
            Toast.makeText(getBaseContext(), "gender is requir", Toast.LENGTH_LONG).show();
            flag = false;
        }

        return flag;
    }

    public String checkFemale() {
        if (rbFemale.isChecked()) {
            name = rbFemale.getText().toString();
        }
        return name;

    }


}